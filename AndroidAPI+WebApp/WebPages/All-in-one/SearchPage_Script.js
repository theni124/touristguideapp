`use strict`;
function checkUser(){
    if(sessionStorage.getItem('username')){
        return;
    }
    else{
        window.location= 'https://192.168.131.234:10029/';
    }
}
checkUser();
function zoomOutMobile() {
    var viewport = document.querySelector('meta[name="viewport"]');

    if ( viewport ) {
        viewport.content = "initial-scale=0.1";
        viewport.content = "width=1200";
    }
}
zoomOutMobile();


function openInNewTab(url) {
    let win = window.open(url, '_blank');
    win.focus();
}

function getAllAttractions(){
    const headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    };
    fetch('https://192.168.131.234:10029/Attractions', {
        headers: headers,
        method: "GET",
        credentials: 'same-origin'
    }).then(response => response.json()).then(data => {
        console.log("Whole data: "+ data)
        data=data.toString();
        let values=data.split(',');
        console.log(values.length);

        var table = document.getElementById("mytable");
        var div=document.getElementById("attractions");
        var txt="";
        //txt += "<table border='1'>"
        txt+="<table id='mytable' class='styled-table'> <thead> <tr> <th>Name</th> <th>Image</th> <th>Rating</th> <th>Position</th> </tr> </thead>"
        txt+="<tbody>"
        for (var i =0;i<values.length;i=i+6){
            //href='https://192.168.131.234:10029/DetailedAttractionPage' onclick='setAttractionID(index)'
            txt+="<tr onclick='setAttractionID("+values[i]+")'>"
            //txt += "<tr><td>" + values[i] + "</td></tr>";
            txt+="<td>"+values[i+1]+"</td>";
            txt+="<td><img width='200px' height='200px' src="+values[i+2]+"></td>";
            txt+="<td>"+values[i+3]+"</td>";
            var pos = values[i+4]+" "+values[i+5];
            txt+="<td>"+pos+"</td>";
            txt+="</tr>"
        }
        txt+="</tbody>"
        txt += "</table>"
        document.getElementById("testdiv").innerHTML="";
        document.getElementById("testdiv").innerHTML=txt;
        //div.appendChild(table);
    }).catch(error => {
        console.log(`There has been a problem with the fetch operation: ${error.message}`);
    })
}

function searchAttractions(){
    const serachfield=document.getElementById("searchtext");
    let searchtext=serachfield.value;
    console.log(searchtext);
    const headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    };
    fetch('https://192.168.131.234:10029/SearchAttraction/'+searchtext, {
        headers: headers,
        method: "GET",
        credentials: 'same-origin'
    }).then(response => response.json()).then(data => {
        console.log("Whole data: "+ data)
        data=data.toString();
        if(data=="false"){
            document.getElementById("testdiv").innerHTML = "";
            var txt="<h2>No attraction found!</h2>"
            document.getElementById("testdiv").innerHTML = txt;
        }
        else {
            let values = data.split(',');
            console.log(values.length);

            var table = document.getElementById("mytable");
            var div = document.getElementById("attractions");
            var txt = "";
            //txt += "<table border='1'>"
            txt += "<table id='mytable' class='styled-table'> <thead> <tr> <th>Name</th> <th>Image</th> <th>Rating</th> <th>Position</th> </tr> </thead>"
            txt += "<tbody>"
            for (var i = 0; i < values.length; i = i + 6) {
                txt += "<tr onclick='setAttractionID(" + values[i] + ")'>"
                //txt += "<tr><td>" + values[i] + "</td></tr>";
                txt += "<td>" + values[i + 1] + "</td>";
                txt += "<td><img width='200px' height='200px' src=" + values[i + 2] + "></td>";
                txt += "<td>" + values[i + 3] + "</td>";
                var pos = values[i + 4] + " " + values[i + 5];
                txt += "<td>" + pos + "</td>";
                txt += "</tr>"
            }
            txt += "</tbody>"
            txt += "</table>"
            document.getElementById("testdiv").innerHTML = "";
            document.getElementById("testdiv").innerHTML = txt;
        }
        //div.appendChild(table);
    }).catch(error => {
        console.log(`There has been a problem with the fetch operation: ${error.message}`);
    })
}

function setAttractionID(index){
    console.log(index);
    sessionStorage.setItem('attractionID',index);
    window.location='https://192.168.131.234:10029/DetailedAttractionPage';
    //openInNewTab('https://192.168.131.234:10029/DetailedAttractionPage');
}


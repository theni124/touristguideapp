`use strict`;
function checkUser(){
    if(sessionStorage.getItem('username')){
        return;
    }
    else{
        window.location= 'https://192.168.131.234:10029/';
    }
}
checkUser();

function zoomOutMobile() {
    var viewport = document.querySelector('meta[name="viewport"]');

    if ( viewport ) {
        viewport.content = "initial-scale=0.1";
        viewport.content = "width=1200";
    }
}
zoomOutMobile();


function getOneAttraction(){
    var attractionID=sessionStorage.getItem('attractionID');
    const headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    };
    fetch('https://192.168.131.234:10029/Attraction/'+attractionID, {
        headers: headers,
        method: "GET",
        credentials: 'same-origin'
    }).then(response => response.json()).then(data => {
        console.log("Whole data: "+ data)
        data=data.toString();
        let values=data.split(',');
        console.log(values.length);

        var header=document.getElementById("attractionHeader");
        var contentdiv=document.getElementById("attractionContent");
        var image=document.getElementById("attractionImage");
        var rateheader=document.getElementById("rateheader");
        header.innerHTML=values[0];
        image.src=values[1];
        contentdiv.innerHTML=values[2];
        rateheader.innerHTML=values[3];

        var txt="";
        //txt += "<table border='1'>"
        /*for (var i =0;i<values.length;i=i+6){
            header.innerHTML=values[i];
            image.src=values[i+1];
            contentdiv.innerHTML=values[i+2];

            txt+="<tr>"
            //txt += "<tr><td>" + values[i] + "</td></tr>";
            txt+="<td>"+values[i]+"</td>";
            txt+="<td><img width='200px' height='200px' src="+values[i+1]+"></td>";
            txt+="<td>"+values[i+2]+"</td>";
            var pos = values[i+3]+" "+values[i+4];
            txt+="<td>"+pos+"</td>";
            txt+="</tr>"
        } */
        //div.appendChild(table);
    }).catch(error => {
        console.log(`There has been a problem with the fetch operation: ${error.message}`);
    })
}

function getAttractionComments(){
    var attractionID= sessionStorage.getItem('attractionID');
    const headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    };
    fetch('https://192.168.131.234:10029/Comments/'+attractionID, {
        headers: headers,
        method: "GET",
        credentials: 'same-origin'
    }).then(response => response.json()).then(data => {
        console.log("Whole data: "+ data)
        data=data.toString();
        if(data=="false"){
            console.log("No comments here yet");
            document.getElementById("commentdiv").innerHTML="";
            var txt= "<ul id='list'><li><p id='authorlabel'>No comments here yet. Be the first one!</p></li></ul>";
            document.getElementById("commentdiv").innerHTML=txt;
        }else {
            let values = data.split(',');
            console.log(values.length);

            var commentdiv = document.getElementById("commentdiv");

            var txt = "";
             for (var i = values.length-1; i>=0; i = i - 3) {

            txt += "<ul id='list'>"
            txt += "<li>"
            //txt += "<tr><td>" + values[i] + "</td></tr>";
            txt += "<p id='authorlabel'>" + values[i-2] + "</p>";
            //txt+="<td><img width='200px' height='200px' src="+values[i+1]+"></td>";
            txt += "<p>" + values[i - 1] + "</p>";
            // var pos = values[i+3]+" "+values[i+4];
            var date = values[i]
            var day = date.substring(0, 10);
            var hour = date.substring(11, 19);
            //System.out.println(day + " " + hour);
            var commentdate = day + " " + hour;
            txt += "<p>" + commentdate + "</p>";
            txt += "</li>"
            txt += "</ul>"
                 console.log("Author:"+values[i-2]);
                 console.log(values[i]);
        }
            //txt += "</ul>"
            commentdiv.innerHTML = txt;
        }
    }).catch(error => {
        console.log(`There has been a problem with the fetch operation: ${error.message}`);
    })
}

function submitComment(){
    const commentfield=document.getElementById("commentfield");
    console.log("eeeeee")
    if (commentfield.value === "") {
        alert("Please write some text for your comment!");
    } else {
        const headers = {
            'Accept': 'application/json',
            //'Content-Type': 'application/json'
            'Content-Type': 'application/json'
        };

        var username= sessionStorage.getItem('username');
        var attractionID= sessionStorage.getItem('attractionID');
        var comment = {
            text: commentfield.value,
            username: username,
            attractionID: attractionID
        }
        fetch('https://192.168.131.234:10029/Comment', {
            headers: headers,
            method: "POST",
            credentials: 'same-origin',
            body: JSON.stringify(comment)
        }).then(response => response.json()).then(data => {
            data=data.toString();
            console.log("data" +data);
            //console.log(data[0][0].false);
            location.reload();

        }).catch(error => {
            console.log(`There has been a problem with the fetch operation: ${error.message}`);


            //window.location.replace("https://students.btsi.lu/theni/CLISS/Project.html")
        })
    }
}
function submitRating(){
    const ratefield=document.getElementById("ratefield");
    console.log("eeeeee")
    if (ratefield.value === "" || isNaN(ratefield.value) || ratefield.value<0 || ratefield.value>5) {
        alert("Please enter a number between 0 and 5!");
    } else {
        const headers = {
            'Accept': 'application/json',
            //'Content-Type': 'application/json'
            'Content-Type': 'application/json'
        };

        var username= sessionStorage.getItem('username');
        var attractionID= sessionStorage.getItem('attractionID');
        var obj = {
            rating: ratefield.value,
            username: username,
            attractionID: attractionID
        }
        fetch('https://192.168.131.234:10029/InsertRating', {
            headers: headers,
            method: "POST",
            credentials: 'same-origin',
            body: JSON.stringify(obj)
        }).then(response => response.json()).then(data => {
            console.log("data" +data);
            //console.log(data[0][0].false);
            location.reload();

        }).catch(error => {
            console.log(`There has been a problem with the fetch operation: ${error.message}`);


            //window.location.replace("https://students.btsi.lu/theni/CLISS/Project.html")
        })
    }
}
addEventListener('load',getAttractionComments);
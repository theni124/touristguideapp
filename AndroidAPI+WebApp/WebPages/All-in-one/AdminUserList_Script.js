`use strict`;
function checkAdmin(){
    if(sessionStorage.getItem('Admin')){
        return;
    }
    else{
        window.location= 'https://192.168.131.234:10029/AdminLoginPage';
    }
}
checkAdmin();

function zoomOutMobile() {
    var viewport = document.querySelector('meta[name="viewport"]');

    if ( viewport ) {
        viewport.content = "initial-scale=0.1";
        viewport.content = "width=1200";
    }
}
zoomOutMobile();
function getAllUsers(){
    const headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    };
    fetch('https://192.168.131.234:10029/GetAllUsers', {
        headers: headers,
        method: "GET",
        credentials: 'same-origin'
    }).then(response => response.json()).then(data => {
        console.log("Whole data: "+ data)
        data=data.toString();
        let values=data.split(',');
        console.log(values.length);

        var table = document.getElementById("mytable");
        var div=document.getElementById("attractions");
        var txt="";
        //txt += "<table border='1'>"
        txt+="<table id='mytable' class='styled-table'> <thead> <tr> <th>UserID</th> <th>Username</th> <th>E-Mail</th></thead>"
        txt+="<tbody>"
        for (var i =0;i<values.length;i=i+3){
            //href='https://192.168.131.234:10029/DetailedAttractionPage' onclick='setAttractionID(index)'
            txt+="<tr onclick='setUserID("+values[i]+")'>";
            //txt += "<tr><td>" + values[i] + "</td></tr>";
            txt+="<td>"+values[i]+"</td>";
            txt+="<td>"+values[i+1]+"</td>";
            txt+="<td>"+values[i+2]+"</td>";
            txt+="</tr>";
        }
        txt+="</tbody>"
        txt += "</table>"
        document.getElementById("testdiv").innerHTML="";
        document.getElementById("testdiv").innerHTML=txt;
        //div.appendChild(table);
    }).catch(error => {
        console.log(`There has been a problem with the fetch operation: ${error.message}`);
    })
}

function setUserID(index){
    console.log(index);
    sessionStorage.setItem('userID',index);
    deleteUser();
}

function deleteUser(){
    if (confirm('Are you sure you want to delete this User and all Comments and Ratings associated with it?')) {
        var id = sessionStorage.getItem('userID');
        var admin=sessionStorage.getItem('Admin');
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };

        var user = {
            userID: id,
            admin:admin
        }
        fetch('https://192.168.131.234:10029/DeleteUser', {
            headers: headers,
            method: "DELETE",
            credentials: 'same-origin',
            body: JSON.stringify(user)
        }).then(response => response.json()).then(data => {
            // console.log("Whole data: "+ data[0][0])
            console.log(data[0][0]);
            if (data[0][0].OK) {
                alert("User deleted!")
                location.reload();
                //window.location.replace('https://192.168.131.234:10029/AdminHomePage');
            }
        }).catch(error => {
            console.log(`There has been a problem with the fetch operation: ${error.message}`);
        })
    }
}

function searchUser(){
    const serachfield=document.getElementById("searchtext");
    let searchtext=serachfield.value;
    console.log(searchtext);
    const headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    };
    fetch('https://192.168.131.234:10029/SearchUser/'+searchtext, {
        headers: headers,
        method: "GET",
        credentials: 'same-origin'
    }).then(response => response.json()).then(data => {
        console.log("Whole data: "+ data)
        data=data.toString();
        if(data=="false"){
            document.getElementById("testdiv").innerHTML = "";
            var txt="<h2>No User found!</h2>"
            document.getElementById("testdiv").innerHTML = txt;
        }
        else {
            let values = data.split(',');
            console.log(values.length);

            var table = document.getElementById("mytable");
            var div = document.getElementById("attractions");
            var txt = "";
            //txt += "<table border='1'>"
            txt+="<table id='mytable' class='styled-table'> <thead> <tr> <th>UserID</th> <th>Username</th> <th>E-Mail</th></thead>"
            txt += "<tbody>"
            for (var i = 0; i < values.length; i = i + 3) {
                txt+="<tr onclick='setUserID("+values[i]+")'>";
                //txt += "<tr><td>" + values[i] + "</td></tr>";
                txt+="<td>"+values[i]+"</td>";
                txt+="<td>"+values[i+1]+"</td>";
                txt+="<td>"+values[i+2]+"</td>";
                txt+="</tr>";
            }
            txt += "</tbody>"
            txt += "</table>"
            document.getElementById("testdiv").innerHTML = "";
            document.getElementById("testdiv").innerHTML = txt;
        }
        //div.appendChild(table);
    }).catch(error => {
        console.log(`There has been a problem with the fetch operation: ${error.message}`);
    })
}

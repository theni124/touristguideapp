`use strict`;
function checkUser(){
    if(sessionStorage.getItem('username')){
        return;
    }
    else{
        window.location= 'https://192.168.131.234:10029/';
    }
}
checkUser();

function zoomOutMobile() {
    var viewport = document.querySelector('meta[name="viewport"]');

    if ( viewport ) {
        viewport.content = "initial-scale=0.1";
        viewport.content = "width=1200";
    }
}
zoomOutMobile();

let userlat="";
let userlong="";
let mymap;
let attraction_array = [];
function getAllAttractionsOnMap(lat,long){
    if (mymap != undefined) { mymap.remove(); }
    var radius=document.querySelector("select");
    radius=parseInt(radius.value);
    console.log(radius);

    console.log(lat+ " "+long);
    userlat=lat;
    userlong=long;
    let lat1=parseFloat(lat);
    let long1=parseFloat(long);
     mymap = L.map('map').setView([lat1,long1], 15);
    console.log("YE");

    var greenIcon = new L.Icon({
        iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
        shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
    });

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoibmVja2VsMzciLCJhIjoiY2tqdmNsMHBsMDc2MjJ1bHNobTIxMHhvYyJ9.rKg71h549vj903ZKWlmU8g'
    }).addTo(mymap);
    var marker = L.marker([lat1, long1], {icon: greenIcon}).addTo(mymap);
    marker.color
    marker.bindPopup("<b>"+"You are here"+"</b>");
    marker.on('mouseover',function(e) {
        e.target.openPopup();
    });

    var circle = L.circle([lat1, long1], {
        color: 'red',
        fillColor: '#f03',
        fillOpacity: 0.5,
        radius: radius
    }).addTo(mymap);
    const headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    };
    fetch('https://192.168.131.234:10029/Attractions', {
        headers: headers,
        method: "GET",
        credentials: 'same-origin'
    }).then(response => response.json()).then(data => {
        console.log("Whole data: "+ data)
        data=data.toString();
        let values=data.split(',')
        // console.dir(data[0][0]);
        for (var i =0;i<values.length;i=i+6){
            //var pos = values[i+3]+" "+values[i+4];
            var point={x:values[i+4], y:values[i+5]};
            console.log(point);
           var dist=distance(point.x,lat1,point.y,long1,0,0);
            //var dist=distance(point.y,point.x,lat1,long1,0,0);
            console.log(dist);
            if(radius>=dist){


           //attraction_array.push(values[i]);
            var id=values[i];
            console.log("ID: "+id);

            var marker = L.marker([values[i+4], values[i+5]]).addTo(mymap);
            marker.myCustomID=values[i];
            var name = values[i+1];
            marker.bindPopup("<b>"+name+"</b>");
            marker.on('mouseover',function(e) {
                e.target.openPopup();
               // console.log("Hi i am "+e.target.myCustomID);
            });
            marker.on('click',function (e) {
                var id = e.target.myCustomID;
                setAttractionID(id);
            });
            }
        }
   // console.log(attraction_array.length);

        //checkDistance();
    }).catch(error => {
        console.log(`There has been a problem with the fetch operation: ${error.message}`);
    })
}

function setAttractionID(index){
    console.log(index);
    sessionStorage.setItem('attractionID',index);
    window.location="https://192.168.131.234:10029/DetailedAttractionPage";
    //openInNewTab('https://192.168.131.234:10029/DetailedAttractionPage');
}

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        prompt("Geolocation is not supported by this browser.");
    }
}
function showPosition(position) {
    //let pos = position.coords.latitude + "," + position.coords.longitude;
    let lat=position.coords.latitude;
    let long=position.coords.longitude;
    getAllAttractionsOnMap(lat,long);
    //let url="https://www.google.com/maps/search/Supermarket/@"+pos+",15.75z";
    //window.open(url);
}

function distance( lat1,  lat2,  lon1,
     lon2,  el1,  el2) {
    Math.radians = function(degrees) {
        return degrees * Math.PI / 180;
    }

   let R = 6371; // Radius of the earth

    var latDistance = Math.radians(lat2-lat1);
    var lonDistance = Math.radians(lon2 - lon1);
    var a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
        + Math.cos(Math.radians(lat1)) * Math.cos(Math.radians(lat2))
        * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    console.log(c);
    var distance = R * c * 1000; // convert to meters

    var height = el1 - el2;

    distance = Math.pow(distance, 2) + Math.pow(height, 2);

    return Math.sqrt(distance);
}



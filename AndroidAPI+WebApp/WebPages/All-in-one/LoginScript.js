`use strict`;

const init=()=> {
    document.querySelector(('#loginbutton')).addEventListener('click', trylogin);
    document.querySelector(('#newreg')).addEventListener('click', register);
    document.querySelector(('#registerbutton')).addEventListener('click', showpanel);
    document.querySelector(('#cancelbutton')).addEventListener('click', closepanel);
    const loginbutton = document.querySelector('#loginbutton');
    const inputname = document.querySelector('#username');
    const inputpassw = document.querySelector('#password');
    const div = document.querySelector('#warning');
    const DEBUG = true;
    const inputnewname = document.querySelector('#newusername');
    const inputnewpassw = document.querySelector('#newpsw');
    const inputnewemail = document.querySelector('#newemail');
    //const wssURL = 'wss://students.btsi.lu/nodetheni/wss';
    //const wss = new WebSocket(wssURL);

    function loadDoc() {
        let xttp = new XMLHttpRequest();
        xttp.addEventListener('load', showdoc);
        xttp.open("GET", "Login_succes");
        xttp.send();
    }


    const showdoc = e => {
        let page = e.target;
        console.dir(e.target);
        //window.open(page);
    };

    function openInNewTab(url) {
        let win = window.open(url, '_blank');
        win.focus();
    }

    function trylogin() {
        console.log("eeeeee")
        if (inputname.value === "") {
            alert("Please enter a username!");
        } else if (inputpassw.value === "") {
            alert("Please enter your password");
        } else {
            div.innerHTML = "";
            let username = inputname.value;
            let password = inputpassw.value;
            // Login(username,password);
            // wss.send(JSON.stringify(inputname.value + "&" + inputpassw.value));
            //window.location.replace("https://students.btsi.lu/theni/CLISS/Project.html")
            const headers = {
                'Accept': 'application/json',
                //'Content-Type': 'application/json'
                'Content-Type': 'application/json'
            };
            /*  var request = new XMLHttpRequest()
              var url='https://students.btsi.lu/nodetheni/Users/'+inputname.value+"&"+inputpassw.value;
              console.log(url);
              request.open('GET', url, true)
              request.onload=function (){
                  var data=JSON.parse(this.response);
                  console.log(data);
                  div.innerHTML=data;
              }
              request.send(); */
            var user = {
                name: inputname.value,
                password: inputpassw.value
            }
            fetch('https://192.168.131.234:10029/Login', {
                headers: headers,
                method: "POST",
                credentials: 'same-origin',
                body: JSON.stringify(user)
            }).then(response => response.json()).then(data => {
                console.log("data" +data);
                //console.log(data[0][0].false);
                if(data==false){
                    alert("Login credentials are invalid!")
                }
                else{
                    sessionStorage.setItem('username',inputname.value);
                    //localStorage.setItem('username',inputname.value);
                    window.location="https://192.168.131.234:10029/SearchPage";
                }

            }).catch(error => {
                console.log(`There has been a problem with the fetch operation: ${error.message}`);


                //window.location.replace("https://students.btsi.lu/theni/CLISS/Project.html")
            })
        }
    }

    function register() {
        var regex=new RegExp('^[a-zA-Z0-9][a-zA-Z0-9._-]*@[a-zA-Z0-9][a-zA-Z0-9._-]*\\.[a-zA-Z]{2,4}$');
        if (inputnewname.value === "" || inputnewpassw.value === "" || inputnewemail.value === "") {
            alert("Please foill out all the fields");
        }
        else if(regex.test(inputnewemail.value)==false){
            alert("E-Mail is not valid!");
        }else if(inputnewpassw.value.length<7){
            alert("Password is too short!");
        }
        else {
            const headers = {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            };
            /*  var request = new XMLHttpRequest()
              var url='https://students.btsi.lu/nodetheni/Users/'+inputname.value+"&"+inputpassw.value;
              console.log(url);
              request.open('GET', url, true)
              request.onload=function (){
                  var data=JSON.parse(this.response);
                  console.log(data);
                  div.innerHTML=data;
              }
              request.send(); */
            var user = {
                username: inputnewname.value,
                password: inputnewpassw.value,
                email: inputnewemail.value
            }
            fetch('https://192.168.131.234:10029/NewUser', {
                headers: headers,
                method: "POST",
                credentials: 'same-origin',
                body: JSON.stringify(user)
            }).then(response => response.json()).then(data => {
               // console.log("Whole data: "+ data[0][0])
                if(data[0][0].false){
                    alert("Registration attempt failed!")
                } else{
                    alert("Success! You can now login!")
                    closepanel();
                }
            }).catch(error => {
                console.log(`There has been a problem with the fetch operation: ${error.message}`);
            })
        }

    };

    let panel = document.getElementById("id01");
    window.onclick = function (event) {
        if (event.target === panel) {
            panel.style.display = "none";
        }
    }

    function showpanel() {
        document.getElementById('id01').style.display = 'block';
        let goTo = document.getElementById('id01');
        goTo.scrollIntoView();

        document.getElementById('registerbutton').style.display = 'none';
        document.getElementById('loginbutton').style.display = 'none';
    }

    function closepanel() {
        document.getElementById('id01').style.display = 'none';
        document.getElementById('registerbutton').style.display = 'flex';
        document.getElementById('loginbutton').style.display = 'flex';
    }
}
addEventListener('load', init);


function gotoForgotPsw(){
    window.location="https://192.168.131.234:10029/ForgotPasswordPage";
}
`use strict`;
function checkAdmin(){
    if(sessionStorage.getItem('Admin')){
        return;
    }
    else{
        window.location= 'https://192.168.131.234:10029/AdminLoginPage';
    }
}
checkAdmin();

function zoomOutMobile() {
    var viewport = document.querySelector('meta[name="viewport"]');

    if ( viewport ) {
        viewport.content = "initial-scale=0.1";
        viewport.content = "width=1200";
    }
}
zoomOutMobile();
function AddAttraction(){
    var attraction_name=document.getElementById("attraction_name");
    var attraction_imagepath=document.getElementById("attraction_imagepath");
    var attraction_content=document.getElementById("attraction_content");
    var attraction_latitude=document.getElementById("attraction_latitude");
    var attraction_longitude=document.getElementById("attraction_longitude");

    if (attraction_name.value === "" || attraction_imagepath.value === "" || attraction_content.value === "" || attraction_latitude.value === "" || attraction_longitude.value === "" || isNaN(attraction_longitude.value) || isNaN(attraction_latitude.value)) {
        alert("Please fill out all the fields");
    } else {
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };

        var attraction = {
            name: attraction_name.value,
            imagepath: attraction_imagepath.value,
            content: attraction_content.value,
            latitude: attraction_latitude.value,
            longitude: attraction_longitude.value
        }
        fetch('https://192.168.131.234:10029/AddAttraction', {
            headers: headers,
            method: "POST",
            credentials: 'same-origin',
            body: JSON.stringify(attraction)
        }).then(response => response.json()).then(data => {
            // console.log("Whole data: "+ data[0][0])
            console.log(data[0][0]);
            if(data[0][0].OK){
                alert("New attraction inserted!");
                window.location.replace('https://192.168.131.234:10029/AdminHomePage');
            }
        }).catch(error => {
            console.log(`There has been a problem with the fetch operation: ${error.message}`);
        })
    }
}
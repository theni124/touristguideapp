`use strict`;
function checkUser(){
    if(sessionStorage.getItem('username')){
        return;
    }
    else{
        window.location= 'https://192.168.131.234:10029/';
    }
}
checkUser();

/*function zoomOutMobile() {
    var viewport = document.querySelector('meta[name="viewport"]');

    if ( viewport ) {
        viewport.content = "initial-scale=0.1";
        viewport.content = "width=1200";
    }
}
zoomOutMobile();*/

function EditUser(){
    const newusernametext=document.getElementById("newusername");
    const newpswtext=document.getElementById("newpsw");
    const newmailtext=document.getElementById("newmail");

    var oldusername = sessionStorage.getItem('username');
    if (newusernametext.value !== "") {
        let newusername = newusernametext.value;
        var user = {
            oldusername : oldusername,
            newusername: newusername
        }
    }
     else if(newpswtext.value !==""){
        let newpassword = newpswtext.value;
        var user = {
            oldusername : oldusername,
            newpassword: newpassword
        }
    }
     else if(newmailtext.value !== ""){
        let newmail = newmailtext.value;
        var user = {
            oldusername : oldusername,
            newmail: newmail
        }
    }

    const headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    };
   /* var user = {
        oldusername : oldusername,
        newusername: newusername,
        newpassword: newpassword,
        newmail: newmail
    } */
    fetch('https://192.168.131.234:10029/EditUser', {
        headers: headers,
        method: "POST",
        credentials: 'same-origin',
        body: JSON.stringify(user)
    }).then(response => response.json()).then(data => {
        console.log(data[0]);
        console.log(data[0][0]);
        if(data[0][0].name_changed){
            alert("Username changed!")
            newusernametext.value='';
        }
        if(data[0][0].password_changed){
            alert("Password changed!")
            newpswtext.value='';
        }
        if(data[0][0].mail_changed){
            alert("E-Mail changed!")
            newmailtext.value='';
        }
        //console.log("String:"+str);
        //alert("Values changed");
        sessionStorage.setItem('username',user.newusername);
        //window.location.replace("https://192.168.131.234:10029/SearchPage")
        //console.log(data.false);
        //console.log(data.dtPassword);
    }).catch(error => {
        console.log(`There has been a problem with the fetch operation: ${error.message}`);


        //window.location.replace("https://students.btsi.lu/theni/CLISS/Project.html")
    })
}

function Logout() {
    var retVal = confirm("Do you really want to Logout?");
    if( retVal == true ) {
        window.location.replace("https://192.168.131.234:10029/")
        localStorage.removeItem('username');
        return true;
    } else {
        //document.write ("User does not want to continue!");
        return false;
    }
}
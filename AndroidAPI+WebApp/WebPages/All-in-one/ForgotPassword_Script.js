`use strict`;

const init=()=> {
    document.querySelector(('#resetbutton')).addEventListener('click', tryResetPsw);
    document.querySelector(('#newreg')).addEventListener('click', resetPassword);
    document.querySelector(('#cancelbutton')).addEventListener('click', goBack);
    const loginbutton = document.querySelector('#loginbutton');
    const inputname = document.querySelector('#username');
    const inputmail = document.querySelector('#email');
    const div = document.querySelector('#warning');
    const DEBUG = true;
    const inputnewpassword = document.querySelector('#newpassword');
    const inputnewpassword2 = document.querySelector('#newpassword2');
    //const wssURL = 'wss://students.btsi.lu/nodetheni/wss';
    //const wss = new WebSocket(wssURL);

    function loadDoc() {
        let xttp = new XMLHttpRequest();
        xttp.addEventListener('load', showdoc);
        xttp.open("GET", "Login_succes");
        xttp.send();
    }


    const showdoc = e => {
        let page = e.target;
        console.dir(e.target);
        //window.open(page);
    };

    function openInNewTab(url) {
        let win = window.open(url, '_blank');
        win.focus();
    }

    function tryResetPsw() {
        console.log("eeeeee")
        if (inputname.value === "") {
            alert("Please enter your username!");
        } else if (inputmail.value === "") {
            alert("Please enter your E-Mail");
        } else {
            div.innerHTML = "";
            let username = inputname.value;
            let password = inputmail.value;
            // Login(username,password);
            // wss.send(JSON.stringify(inputname.value + "&" + inputpassw.value));
            //window.location.replace("https://students.btsi.lu/theni/CLISS/Project.html")
            const headers = {
                'Accept': 'application/json',
                //'Content-Type': 'application/json'
                'Content-Type': 'application/json'
            };
            var user = {
                name: inputname.value,
                mail: inputmail.value
            }
            fetch('https://192.168.131.234:10029/ValidInfo', {
                headers: headers,
                method: "POST",
                credentials: 'same-origin',
                body: JSON.stringify(user)
            }).then(response => response.json()).then(data => {
                console.log("data" +data);
                console.log(data[0][0]);
                //console.log(data[0][0].false);
               if(data[0][0].false){
                    alert("Invalid User Credentials!")
                }
                else{
                   // alert("Correct");
                   showpanel();
                   //localStorage.setItem('username',inputname.value);
                   sessionStorage.setItem('username',inputname.value);
                }

            }).catch(error => {
                console.log(`There has been a problem with the fetch operation: ${error.message}`);
            })
        }
    }

    function resetPassword() {
        if (inputnewpassword.value === "" || inputnewpassword2.value === "") {
            alert("Please fill out all the fields");
        } else if(inputnewpassword.value !=inputnewpassword2.value){
            alert("Passwords do not match!");
        }
        else if(inputnewpassword.value.length<7 || inputnewpassword2.value<7){
            alert("Password must be at least 7 charachters long!");
        }
        else
        {
            var username=sessionStorage.getItem('username');
            const headers = {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            };
            var user = {
                name: username,
                password: inputnewpassword2.value
            }
            fetch('https://192.168.131.234:10029/ResetPassword', {
                headers: headers,
                method: "POST",
                credentials: 'same-origin',
                body: JSON.stringify(user)
            }).then(response => response.json()).then(data => {
                // console.log("Whole data: "+ data[0][0])
                if(data[0][0].password_changed){
                    alert("Password changed!")
                    window.location.replace("https://192.168.131.234:10029/")
                }
            }).catch(error => {
                console.log(`There has been a problem with the fetch operation: ${error.message}`);
            })
        }

    };

    function goBack(){
        window.location = 'https://192.168.131.234:10029/';
    }

    let panel = document.getElementById("id01");
    window.onclick = function (event) {
        if (event.target === panel) {
            panel.style.display = "none";
        }
    }

    function showpanel() {
        document.getElementById('id01').style.display = 'block';
        let goTo = document.getElementById('id01');
        goTo.scrollIntoView();

        document.getElementById('registerbutton').style.display = 'none';
        document.getElementById('loginbutton').style.display = 'none';
    }

    function closepanel() {
        document.getElementById('id01').style.display = 'none';
        document.getElementById('registerbutton').style.display = 'flex';
        document.getElementById('loginbutton').style.display = 'flex';
    }
}
addEventListener('load', init);

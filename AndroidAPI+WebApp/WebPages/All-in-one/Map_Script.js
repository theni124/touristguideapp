`use strict`;
function checkUser(){
    if(sessionStorage.getItem('username')){
        return;
    }
    else{
        window.location= 'https://192.168.131.234:10029/';
    }
}
checkUser();

function zoomOutMobile() {
    var viewport = document.querySelector('meta[name="viewport"]');

    if ( viewport ) {
        viewport.content = "initial-scale=0.1";
        viewport.content = "width=1200";
    }
}
zoomOutMobile();

const init=()=> {
    getAllAttractionsOnMap();
}
function getAllAttractionsOnMap(){
    var mymap = L.map('map').setView([49.611827, 6.129046], 15);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoibmVja2VsMzciLCJhIjoiY2tqdmNsMHBsMDc2MjJ1bHNobTIxMHhvYyJ9.rKg71h549vj903ZKWlmU8g'
    }).addTo(mymap);
    const headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    };
    fetch('https://192.168.131.234:10029/Attractions', {
        headers: headers,
        method: "GET",
        credentials: 'same-origin'
    }).then(response => response.json()).then(data => {
        console.log("Whole data: "+ data)
        data=data.toString();
        let values=data.split(',')
        // console.dir(data[0][0]);
        for (var i =0;i<values.length;i=i+6){
            //var pos = values[i+3]+" "+values[i+4];
            var id=values[i];
            console.log("ID: "+id);
           /* var marker = L.marker([values[i+4], values[i+5]]).addTo(mymap).on('click', function(e) {
                //alert(e.latlng);
                setAttractionID(e.id)
                //localStorage.setItem('attractionID',id);
                //window.location.replace("https://192.168.131.234:10029/DetailedAttractionPage")
            });*/
            var marker = L.marker([values[i+4], values[i+5]]).addTo(mymap);
            marker.myCustomID=values[i];
            var name = values[i+1];
            marker.bindPopup("<b>"+name+"</b>");
            marker.on('mouseover',function(e) {
                e.target.openPopup();
                console.log("Hi i am "+e.target.myCustomID);
            });
            marker.on('click',function (e){
                var id=e.target.myCustomID;
                setAttractionID(id);
            });
        }

    }).catch(error => {
        console.log(`There has been a problem with the fetch operation: ${error.message}`);
    })
}

function setAttractionID(index){
    console.log(index);
    sessionStorage.setItem('attractionID',index);
    window.location="https://192.168.131.234:10029/DetailedAttractionPage";
    //openInNewTab('https://192.168.131.234:10029/DetailedAttractionPage');
}

addEventListener('load', init);
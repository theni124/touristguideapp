package com.example.loginapplication;

import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private BottomNavigationView bottomNavigationView;
//    String username=getIntent().getStringExtra("username");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomNavigationView=findViewById(R.id.navbar);
        bottomNavigationView.setOnNavigationItemSelectedListener(navbaritemselected);
        getSupportFragmentManager().beginTransaction().replace(R.id.container,new AroundmeFragment()).commit();
    }

    //Actions of the Navigation bar
    private  BottomNavigationView.OnNavigationItemSelectedListener navbaritemselected=new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment=null;
            switch (item.getItemId()){
                case R.id.aroundme:
                    fragment = new AroundmeFragment();
                    break;

                case R.id.search:
                    fragment = new SearchFragment();
                    break;

                case R.id.map:
                    fragment = new MapFragment();
                    break;

                case R.id.settings:
                    fragment = new SettingsFragment();
                    break;
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.container,fragment).commit();

            return true;
        }
    };

    public String getUsername(){
        return "no username";
    }
}
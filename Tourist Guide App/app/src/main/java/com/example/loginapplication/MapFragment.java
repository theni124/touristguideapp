package com.example.loginapplication;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MapFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapFragment extends Fragment {

    GoogleMap map;
    ArrayList<Attraction> attraction_array=new ArrayList<>();
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public MapFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MapFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MapFragment newInstance(String param1, String param2) {
        MapFragment fragment = new MapFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_map, container, false);
        //Define the map on the view
        SupportMapFragment mapFragment= (SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.map);
        //Ate the beggining move camera over Luxembourg and display a marker for each attraction
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                googleMap.moveCamera( CameraUpdateFactory.newLatLngZoom(new LatLng(49.60916073574072, 6.134068849572238) , 15.0f) );
                try {
                    getAttractions();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                for (int i=0;i<attraction_array.size();i++){
                    LatLng pos= new LatLng(attraction_array.get(i).getLongitude(),attraction_array.get(i).getLatitude());
                    String name = attraction_array.get(i).getName();
                    Marker marker=googleMap.addMarker(new MarkerOptions().position(pos)
                            .title(name));
                    int id=attraction_array.get(i).getID();
                    marker.setTag(id);
                }

                //Action Event for a marker
                googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        marker.showInfoWindow();
                        return true;
                    }
                });
                //Action Event for a click on the info window of a marker
                googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        int attractionID = (int) marker.getTag();
                        goToAttraction(attractionID);
                    }
                });

            }
        });
        return v;
    }

        //opens the AttractionActivity with the attractionID as an intent
    public void goToAttraction(int attractionID){
        Intent intent = new Intent (getActivity(), AttractionActivity.class);
        intent.putExtra("AttractionID",attractionID);
        startActivity(intent);
    }

    //retrieves the attractions from the API with a GET request
    public void getAttractions() throws InterruptedException {
        Runnable r= new Runnable() {
            @Override
            public void run() {
                try{
                    URL url=new URL("https://192.168.131.234:10029/Attractions");
                    disableSSLCertificateChecking();
                    HttpURLConnection urlConnection=(HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("GET");
                    System.out.println("Start");
                    try {
                        BufferedReader reader=new BufferedReader(new InputStreamReader(new BufferedInputStream(urlConnection.getInputStream())));
                        StringBuffer sb=new StringBuffer();
                        String str;
                        while((str=reader.readLine())!=null){
                            System.out.println("Haihinn");
                            System.out.println(str);
                            str=str.replace("{","");
                            str=str.replace("[","");
                            str=str.replace("]","");
                            str=str.replaceAll("\"","");
                            String[] values= str.split(",");
                            for (int i=0;i<values.length;i=i+6){
                                Attraction attraction=new Attraction(values[i+1],values[i+2],Double.valueOf(values[i+3]),Double.valueOf(values[i+4]),Double.valueOf(values[i+5]));
                                System.out.println("Attraction is "+attraction.getName());
                                attraction.setID(Integer.parseInt(values[i]));
                                attraction_array.add(attraction);
                            }
                            sb.append(str);
                        }
                        System.out.println("Over here");
                        System.out.println("End");
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread t=new Thread(r);
        t.start();
        t.join();
    }

    private static void disableSSLCertificateChecking() {
        TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {

                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {
                        // not implemented
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {
                        // not implemented
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                }
        };

        try {

            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {

                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }

            });
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}
package com.example.loginapplication;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AroundmeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AroundmeFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    EditText radiustxt;
    GoogleMap map;
    Button radiusbutton;
    Spinner myspinner;
    FusedLocationProviderClient client;
    ArrayList<Attraction> attraction_array = new ArrayList<>();

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public AroundmeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AroundmeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AroundmeFragment newInstance(String param1, String param2) {
        AroundmeFragment fragment = new AroundmeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_aroundme, container, false);
        try {
            getAttractions();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Size: "+attraction_array.size());
        //Creates a spinner at the top of the view with a custom arraylist
        myspinner = v.findViewById(R.id.myspinner);
        ArrayAdapter<CharSequence> adapter= ArrayAdapter.createFromResource(getContext(),R.array.radiusarray, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        myspinner.setAdapter(adapter);
       myspinner.setOnItemSelectedListener(this);


        //  Ask for Localization
        client = LocationServices.getFusedLocationProviderClient(getActivity());

        if (ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            getCurrentLocation();
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 44);
        }
        return v;
    }
    //This method make a GET request to the getAllAttraction method in the API to retrieve the information about each attraction
    public void getAttractions() throws InterruptedException {
        Runnable r = new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL("https://192.168.131.234:10029/Attractions");
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("GET");
                    System.out.println("Start");
                    String[] values = new String[0];
                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(urlConnection.getInputStream())));
                        StringBuffer sb = new StringBuffer();
                        String str;
                        while ((str = reader.readLine()) != null) {
                            System.out.println("Haihinn");
                            System.out.println(str);
                            str = str.replace("{", "");
                            str = str.replace("[", "");
                            str = str.replace("]", "");
                            str = str.replaceAll("\"", "");
                            values = str.split(",");
                            //System.out.println(values[0]+ " "+values[1]+" "+values[5]);
                            sb.append(str);
                            for (int i=0;i<values.length;i=i+6){
                                Attraction attraction=new Attraction(values[i+1],values[i+2],Double.valueOf(values[i+3]),Double.valueOf(values[i+4]),Double.valueOf(values[i+5]));
                                System.out.println("Attraction is "+attraction.getName());
                                attraction_array.add(attraction);
                            }
                        }
                        System.out.println("Over here");
                        System.out.println("End");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread t = new Thread(r);
        t.start();
        t.join();
    }

        //Get the current location if access is granted
    private void getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Task<Location> task = client.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    System.out.println("ZE");
                    SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
                    mapFragment.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(GoogleMap googleMap) {
                            // Create a marker on the map for each attraction
                            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                            MarkerOptions markerOptions = new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).position(latLng).title("You are here");

                            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                            googleMap.addMarker(markerOptions);
                        }
                    });
                }
            }
        });
    }
    //This method draws a circle on the map around the user's position with a certain radius
    public void drawCircleOnMap(int radius) {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Task<Location> task = client.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if(location!=null){
                    System.out.println("ZE");
                    SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
                    mapFragment.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(GoogleMap googleMap) {
                            System.out.println("vasy");
                            googleMap.clear();
                           LatLng latLng= new LatLng(location.getLatitude(),location.getLongitude());
                            MarkerOptions markerOptions= new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).position(latLng).title("You are here");
                            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,15));
                            googleMap.addMarker(markerOptions);

                          CircleOptions circleOptions = new CircleOptions().center(latLng).radius(radius);
                          Circle circle= googleMap.addCircle(circleOptions);
                          circle.setStrokeColor(Color.RED);
                          //checks if an attraction isinside of the circle and if true a marker is placed
                            for (int i = 0; i < attraction_array.size(); i++) {
                               LatLng attractionPoint = new LatLng(attraction_array.get(i).getLongitude(),attraction_array.get(i).getLatitude());

                                double lat1= attractionPoint.longitude;
                                double long1= attractionPoint.latitude;
                                double lat2=latLng.latitude;
                                double long2=latLng.longitude;
                                double dist= distance(long1,lat2,lat1,long2,0,0);
                                if(radius>=dist){
                                    String name = attraction_array.get(i).getName();
                                    Marker marker= googleMap.addMarker(new MarkerOptions().position(attractionPoint)
                                            .title(name));
                                    marker.setTag(i+1);
                                    //Action event for a marker
                                    googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                                        @Override
                                        public void onInfoWindowClick(Marker marker) {
                                            int attractionID = (int) marker.getTag();
                                            goToAttraction(attractionID);
                                        }
                                    });
                                }
                                System.out.println("Distance: "+dist);
                            }
                        }
                    });
                }
            }
        });
    }

    //opens the AttractionActivity with the attractionID as an intent
    public void goToAttraction(int attractionID){
        Intent intent = new Intent (getActivity(), AttractionActivity.class);
        intent.putExtra("AttractionID",attractionID);
        startActivity(intent);
    }

    //Method that calculates the distance between two points
    public static double distance(double lat1, double lat2, double lon1,
                                  double lon2, double el1, double el2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        System.out.println("C is "+c);
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }

    /*function arePointsNear(checkPoint, centerPoint, km) {
        var ky = 40000 / 360;
        var kx = Math.cos(Math.PI * centerPoint.lat / 180.0) * ky;
        var dx = Math.abs(centerPoint.lng - checkPoint.lng) * kx;
        var dy = Math.abs(centerPoint.lat - checkPoint.lat) * ky;
        return Math.sqrt(dx * dx + dy * dy) <= km;
    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==44){
            if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                getCurrentLocation();
            }
        }
    }

    public void showMessage(String title, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
        builder.setTitle(title);
        builder.setMessage(msg);

        builder.setCancelable(false);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //goToLogin();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text=parent.getItemAtPosition(position).toString();
        String[] array= text.split(" ");
        int radius;
        if(array[1].equals("m")){
            radius=Integer.parseInt(array[0]);
        }
        else{
            radius=Integer.valueOf(array[0])*1000;
        }
        System.out.println("Radius is "+radius);
        drawCircleOnMap(radius);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
package com.example.loginapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AttractionAdapter extends ArrayAdapter<Attraction> {
    private Context mContext;
    private int mResource;
    public AttractionAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Attraction> objects) {
        super(context, resource, objects);
        this.mContext=context;
        this.mResource=resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater=LayoutInflater.from(mContext);

        convertView = layoutInflater.inflate(mResource,parent,false);

        //imageView.setImageResource();
        TextView textView = convertView.findViewById(R.id.mytext);
        textView.setText(getItem(position).getName());

        TextView starview = convertView.findViewById(R.id.mystartext);
        starview.setText(getItem(position).getStarRate());

        ImageView imageView= convertView.findViewById(R.id.myimageview);
        imageView.setMinimumWidth(150);
        imageView.setMinimumHeight(150);
        Picasso.get().load(getItem(position).getImagepath()).into(imageView);

        return convertView;

    }
    public void filterList(ArrayList<Attraction> filteredlist){
    }
}

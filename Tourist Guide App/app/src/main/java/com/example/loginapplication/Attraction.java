package com.example.loginapplication;

public class Attraction {
    private String name;
    private String imagepath;
    private double rating;
    private double longitude;
    private double latitude;
    private int id;

    public Attraction(String pName,String pImagepath,double pRating,double pLong,double pLat){
        name=pName;
        imagepath=pImagepath;
        rating=pRating;
        longitude=pLong;
        latitude=pLat;
    }


    public String getName(){
        return name;
    }

    public String getImagepath(){
        return imagepath;
    }

    public String getStarRate(){
        return String.valueOf(rating);
    }

    public double getLongitude(){return longitude;}

    public double getLatitude(){return latitude;}

    public int getID(){return id;};
    public void setID(int newid){
        id=newid;
    }


}

package com.example.loginapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class ForgotPasswordActivity extends AppCompatActivity {

    Button cancelButton,continueButton;
    TextView text;
    EditText mailtextfield,usernametextfield;
    private String mail;
    private String username;
    Context forgotActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        // Defining the values for the input textfields and buttons
        forgotActivity=this;
        text=(TextView) findViewById(R.id.textfield);
        cancelButton = (Button) findViewById(R.id.cancelbutton);
        usernametextfield  = (EditText) findViewById(R.id.usernameTextField);
        mailtextfield = (EditText) findViewById(R.id.mailTextField);
        continueButton = (Button) findViewById(R.id.continuebutton);
        //Action event for the continue Button
       continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username = usernametextfield.getText().toString();
                mail = mailtextfield.getText().toString();
                if(username.equals("") || mail.equals("")){
                    text.setText("Fill out the fields");
                }
                else {
                    try {
                        UIUtil.hideKeyboard(ForgotPasswordActivity.this);
                        validInfo(username,mail);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });



        //Action event for the cancel Button
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToLogin();
            }
        });
    }


    //go back to the Login Activity
    public void goToLogin() {
        Intent intent = new Intent (this, LoginActivity.class);
        startActivity(intent);
    }

    //Continue to the Reset Password Activity
    public void goToReset(){
        Intent intent = new Intent (this, ResetPasswordActivity.class);
        startActivity(intent);
    }

    //This method make a POST request to the ValidInfo method in the API with the username and mail inside the body of the request
    public void validInfo(String username,String mail) throws JSONException {
        JSONObject json=new JSONObject();
        json.put("name",username);
        json.put("mail",mail);
        String jsonString = json.toString();

        Runnable r=new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("Sending...");
                    //URL url= new URL("https://students.btsi.lu/nodetheni/Login");
                    URL url=new URL("https://192.168.131.234:10029/ValidInfo");

                    disableSSLCertificateChecking();

                    HttpsURLConnection urlConnection=(HttpsURLConnection) url.openConnection();
                    urlConnection.getSSLSocketFactory();
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setDoOutput(true);
                    urlConnection.addRequestProperty("Accept", "application/json");
                    urlConnection.addRequestProperty("Content-Type", "application/json");

                    OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
                    writer.write(jsonString);
                    writer.flush();
                    writer.close();
                    out.close();
                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(urlConnection.getInputStream())));
                        StringBuffer sb = new StringBuffer();
                        String str;
                        while ((str = reader.readLine()) != null) {
                            System.out.println(str);
                            str=str.replace("{","");
                            str=str.replace("}","");
                            str=str.replace("[","");
                            str=str.replace(":","");
                            str=str.replace("]","");
                            str=str.replaceAll("\"","");
                            str=str.replace("dtPassword","");
                            String[] values= str.split(",");
                            str=values[0];
                            System.out.println(str);
                            sb.append(str);
                        }
                        runOnUiThread(new Runnable(){

                            @Override
                            public void run() {
                                // hashedpsw=sb.toString();
                                //decrypt(hashedpsw);
                                if(sb.toString().contains("false")){
                                    text.setText("Invalid Credentials!");
                                }
                                else{
                                    //decrypt(sb.toString());
                                    goToReset();
                                    saveUsername(username);
                                }
                            }
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // urlConnection.connect();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        Thread t=new Thread(r);
        t.start();
    }

    public void saveUsername(String username){
        SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor myEditor = myPreferences.edit();
        myEditor.putString("Username", username);
        myEditor.commit();
    }



    private static void disableSSLCertificateChecking() {
        TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {

                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {
                        // not implemented
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {
                        // not implemented
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                }
        };

        try {

            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {

                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }

            });
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}
package com.example.loginapplication;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.squareup.picasso.Picasso;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchFragment extends Fragment {
    ListView searchlistview;
    ArrayList<Attraction> attraction_array=new ArrayList<>();
    MainActivity activity = (MainActivity) getActivity();
    Button searchbutton;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public SearchFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SearchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SearchFragment newInstance(String param1, String param2) {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_search, container, false);
        //Define the elements on the view
        searchlistview=(ListView) v.findViewById(R.id.searchlistview);
        //List attractions on the list
        try {
            System.out.println("Retrieve Attractions");
            getAttractions();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
         AttractionAdapter attractionAdapter= new AttractionAdapter(getActivity(), R.layout.list_row,attraction_array);
         searchlistview.setAdapter(attractionAdapter);
         //Action evnt for a list item
        searchlistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Attraction attraction=attraction_array.get(position);
                    int attractionID=attraction.getID();
                    goToAttraction(attractionID);
            }
        });
        EditText editText=v.findViewById(R.id.searchtext);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Editable editable=editText.getText();
                String str=editable.toString();
                if (str != null) {
                    attraction_array.clear();
                    try {
                        getSearchAttractions(s.toString());
                        attractionAdapter.notifyDataSetChanged();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }/* else{
                    try {
                        getAttractions();
                        attractionAdapter.notifyDataSetChanged();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } */
            }
        });
        searchbutton = v.findViewById(R.id.searchbutton);
        searchbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UIUtil.hideKeyboard(getActivity());
            }
        });

        return v;
    }

    public void getAttractions() throws InterruptedException {
        Runnable r= new Runnable() {
            String[] values;
            @Override
            public void run() {
                try{
                   // URL url=new URL("https://students.btsi.lu/nodetheni/Attractions");
                    URL url=new URL("https://192.168.131.234:10029/Attractions");
                    disableSSLCertificateChecking();
                    HttpURLConnection urlConnection=(HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("GET");
                    System.out.println("Start");
                    try {
                        BufferedReader reader=new BufferedReader(new InputStreamReader(new BufferedInputStream(urlConnection.getInputStream())));
                        StringBuffer sb=new StringBuffer();
                        String str;
                        while((str=reader.readLine())!=null){
                            System.out.println("Haihinn");
                            System.out.println(str);
                            str=str.replace("{","");
                            str=str.replace("[","");
                            str=str.replace("]","");
                            /*str=str.replace("idAttraction","");
                            str=str.replace("dtName","");
                            str=str.replace("dtImagePath","");
                            str=str.replace("dtContent","");
                            str=str.replace("dtRating","");
                            str=str.replace("dtLongitude","");
                            str=str.replace("dtLatitude",""); */
                            str=str.replaceAll("\"","");
                            System.out.println(str);
                            String[] values= str.split(",");
                            for (int i=0;i<values.length;i=i+6){
                                Attraction attraction=new Attraction(values[i+1],values[i+2],Double.valueOf(values[i+3]),Double.valueOf(values[i+4]),Double.valueOf(values[i+5]));
                                System.out.println("Attraction is "+attraction.getName());
                                attraction.setID(Integer.parseInt(values[i]));
                                attraction_array.add(attraction);
                            }
                            //System.out.println(values[0]+ " "+values[1]+" "+values[5]);
                            values=str.split(",");
                            sb.append(str);
                        }
                        System.out.println("Over here");
                        getActivity().runOnUiThread(new Runnable(){

                            @Override
                            public void run() {
                                // hashedpsw=sb.toString();
                                //decrypt(hashedpsw);
                                /*for (int i=0;i<values.length;i=i+6){
                                    Attraction attraction=new Attraction(values[i+1],values[i+2],Double.valueOf(values[i+3]),Double.valueOf(values[i+4]),Double.valueOf(values[i+5]));
                                    System.out.println("Attraction is "+attraction.getName());
                                    attraction.setID(Integer.parseInt(values[i]));
                                    attraction_array.add(attraction);
                                } */

                            }
                        });
                        System.out.println("End");
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread t=new Thread(r);
        t.start();
        t.join();
    }

    public void goToAttraction(int attractionID){
        Intent intent = new Intent (getActivity(), AttractionActivity.class);
        intent.putExtra("AttractionID",attractionID);
        //intent.putExtra("Username",activity.getUsername());
        startActivity(intent);
    }

    public void getSearchAttractions(String txt) throws InterruptedException{

        String text=txt;
        System.out.println("Text is "+text);
        if(text.isEmpty()){
           // System.out.println("jaja");
            getAttractions();
        }
        else {
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    String[] values = new String[0];
                    try {
                        //URL url=new URL("https://students.btsi.lu/nodetheni/SearchAttraction/"+txt);
                        URL url = new URL("https://192.168.131.234:10029/SearchAttraction/" + text);
                        disableSSLCertificateChecking();
                        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                        urlConnection.setRequestMethod("GET");
                        System.out.println("Start");
                        try {
                            BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(urlConnection.getInputStream())));
                            StringBuffer sb = new StringBuffer();
                            String str;
                            while ((str = reader.readLine()) != null) {
                                System.out.println("Haihinn");
                                System.out.println(str);
                                str = str.replace("{", "");
                                str = str.replace("[", "");
                                str = str.replace("]", "");
                            /*str=str.replace("idAttraction","");
                            str=str.replace("dtName","");
                            str=str.replace("dtImagePath","");
                            str=str.replace("dtContent","");
                            str=str.replace("dtRating","");
                            str=str.replace("dtLongitude","");
                            str=str.replace("dtLatitude",""); */
                                str = str.replaceAll("\"", "");
                                System.out.println(str);
                                values = str.split(",");
                                //System.out.println(values[0]+ " "+values[1]+" "+values[5]);
                                sb.append(str);
                                if (str.equals("false")) {
                                    System.out.println("ERROR");
                                    return;
                                }
                            }
                            System.out.println("Over here");
                            String[] finalValues = values;
                            getActivity().runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    // hashedpsw=sb.toString();
                                    //decrypt(hashedpsw);
                                    if (finalValues[0] != null) {
                                        for (int i = 0; i < finalValues.length; i = i + 6) {
                                            Attraction attraction = new Attraction(finalValues[i + 1], finalValues[i + 2], Double.valueOf(finalValues[i + 3]), Double.valueOf(finalValues[i + 4]), Double.valueOf(finalValues[i + 5]));
                                            System.out.println("Attraction is " + attraction.getName());
                                            attraction.setID(Integer.parseInt(finalValues[i]));
                                            attraction_array.add(attraction);

                                        }
                                    }

                                }
                            });
                            System.out.println("End");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            };
            Thread t = new Thread(r);
            t.start();
            t.join();
        }
    }

    private static void disableSSLCertificateChecking() {
        TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {

                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {
                        // not implemented
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {
                        // not implemented
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                }
        };

        try {

            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {

                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }

            });
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

   /* public void filter(String text){
        ArrayList<Attraction> filtered=new ArrayList<>();
        for(Attraction attraction : attraction_array){
            if(attraction.getName().toLowerCase().contains(text.toLowerCase())){
                filtered.add(attraction);
            }
        }
        AttractionAdapter.filterList(filtered);
    } */
}
package com.example.loginapplication;

//6package com.example.loginapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.text.Spannable;
import android.text.method.MovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.loginapplication.Comment;
import com.example.loginapplication.MainActivity;
import com.squareup.picasso.Picasso;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.CookieHandler;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class AttractionActivity extends AppCompatActivity {
    ArrayList<Comment> comments=new ArrayList<>();
    RecyclerView myrecycler;
    Button backbutton;
    Button ratebutton;
    Button postButton;
    EditText comment;
    String rate;
    Integer attractionID;
    TextView contentText;
    TextView rateText;
    ImageView attractionimage;
    Dialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.activity_attraction);
        attractionID= getIntent().getIntExtra("AttractionID",0);
        int ID=Integer.valueOf(attractionID);
        System.out.println("MY ID is "+ID);
        try {
            getComments(ID);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
       /* comments.add(new Comment("Neckel","moin moin"));
        comments.add(new Comment("Nigo","waat leeft"));
        comments.add(new Comment("Ivan","fuuzer mec"))
        comments.add(new Comment("Vincent","fuuzer mec"));
        comments.add(new Comment("Jeff","fuuzer mec")); */
        myrecycler=  findViewById(R.id.myrecycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
       myrecycler.setLayoutManager(layoutManager);
        CommentAdapter commentAdapter= new CommentAdapter(comments);
        myrecycler.setAdapter(commentAdapter);
       // SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(AttractionActivity.this);
        //String name = myPreferences.getString("Username", "unknown");
       // System.out.println("Your name is "+name);
        backbutton= findViewById(R.id.backbutton);
        ratebutton= findViewById(R.id.ratebutton);
        postButton = findViewById(R.id.postbutton);
        comment = findViewById(R.id.commentText);
        contentText= findViewById(R.id.content);
        contentText.setMovementMethod(new ScrollingMovementMethod());
        rateText=findViewById(R.id.rateText);
        attractionimage = findViewById(R.id.attractionimage);
        //String username= getIntent().getStringExtra("Username");
        //System.out.println("You are "+username);
        try {
            getAttractionDetails(attractionID);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        attractionimage.setClickable(true);
        //View view=getLayoutInflater().inflate(R.layout.activity_attraction,null);
        //dialog=new Dialog(this, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
        //dialog.setContentView(view);
        System.out.println("ID :"+attractionID);
        //textview.setText(attractionID);
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });
        ratebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    openDialog();
            }
        });

        postButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str=comment.getText().toString();
                System.out.println(str);
                if(str.matches("")){
                    //Erroor Dialog
                }
                else {
                    try {
                        submitComment(str);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void goBack(){
       // Intent intent = new Intent (this, MainActivity.class);
        //startActivity(intent);
        super.onBackPressed();

    }

    public void openDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Enter a value between 0 and 5 (No Decimal)");

// Set up the input
        final EditText input = new EditText(this);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        builder.setView(input);

// Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                rate = input.getText().toString();
                if(rate.matches("")){
                    openDialog();
                }
                else {
                    int intrate = Integer.valueOf(rate);
                    if (intrate < 0 || intrate > 5) {
                        openDialog();
                    }
                    else {
                        try {
                            submitRating(intrate);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void getAttractionDetails(int attractionID) throws InterruptedException {
        Runnable r=new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("Sending...");
                    //URL url= new URL("https://students.btsi.lu/nodetheni/Attraction/"+attractionID);
                    URL url=new URL("https://192.168.131.234:10029/Attraction/"+attractionID);
                    HttpURLConnection urlConnection=(HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("GET");

                    /*OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
                    writer.flush();
                    writer.close();
                    out.close();  */
                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(urlConnection.getInputStream())));
                        StringBuffer sb = new StringBuffer();
                        String str;
                        while ((str = reader.readLine()) != null) {
                            System.out.println(str);
                            str=str.replace("{","");
                            str=str.replace("[","");
                            str=str.replace("]","");
                            str=str.replaceAll("\"","");
                            String[] values= str.split(",");
                            System.out.println(values);

                           // Picasso.get().load(values[2]).into(attractionimage);
                            contentText.setText(values[2]);
                            rateText.setText(values[3]);
                            sb.append(values[1]);
                        }
                        runOnUiThread(new Runnable(){

                            @Override
                            public void run() {
                                // hashedpsw=sb.toString();
                                //decrypt(hashedpsw);
                                String imagepath=sb.toString();
                                System.out.println(imagepath);
                                Picasso.get().load(imagepath).into(attractionimage);
                            }
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // urlConnection.connect();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        Thread t=new Thread(r);
        t.start();
        t.join();
    }

    public void submitRating(int rate) throws JSONException {
        SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(AttractionActivity.this);
        String name = myPreferences.getString("Username", "unknown");
        JSONObject json = new JSONObject();
        // String name = myPreferences.getString("NAME", "unknown");
        json.put("rating", rate);
        json.put("username", name);
        json.put("attractionID", attractionID);
        String jsonstring = json.toString();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("Sending...");
                    // URL url= new URL("https://students.btsi.lu/nodetheni/NewUser");
                    URL url = new URL("https://192.168.131.234:10029/InsertRating");
                    disableSSLCertificateChecking();
                    HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setDoOutput(true);
                    urlConnection.addRequestProperty("Accept", "application/json");
                    urlConnection.addRequestProperty("Content-Type", "application/json");

                    OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
                    writer.write(jsonstring);
                    writer.flush();
                    writer.close();
                    out.close();
                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(urlConnection.getInputStream())));
                        StringBuffer sb = new StringBuffer();
                        String str;
                        while ((str = reader.readLine()) != null) {
                            str = str.replace("{", "");
                            str = str.replace("}", "");
                            str = str.replace("[", "");
                            str = str.replace("]", "");
                            str = str.replaceAll("\"", "");
                            System.out.println(str);
                            sb.append(str);
                        }
                     /*   getActivity().runOnUiThread(new Runnable(){

                            @Override
                            public void run() {
                                // hashedpsw=sb.toString();
                                //decrypt(hashedpsw);
                                if(sb.toString().contains("false")){
                                    invalidReg("Error","Invalid Credentials!");
                                }
                                else{
                                    showalert();
                                }
                            }
                        }); */
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // urlConnection.connect();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        Thread t = new Thread(r);
        t.start();
        finish();
        startActivity(getIntent());
    }
    public void submitComment(String text) throws JSONException {
        SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(AttractionActivity.this);
        String name = myPreferences.getString("Username", "unknown");
        JSONObject json = new JSONObject();
        System.out.println(text);
        // String name = myPreferences.getString("NAME", "unknown");
        json.put("text", text);
        json.put("username", name);
        json.put("attractionID", attractionID);
        String jsonstring = json.toString();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("Sending...");
                    // URL url= new URL("https://students.btsi.lu/nodetheni/NewUser");
                    URL url = new URL("https://192.168.131.234:10029/Comment");
                    disableSSLCertificateChecking();
                    HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setDoOutput(true);
                    urlConnection.addRequestProperty("Accept", "application/json");
                    urlConnection.addRequestProperty("Content-Type", "application/json");

                    OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
                    writer.write(jsonstring);
                    writer.flush();
                    writer.close();
                    out.close();
                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(urlConnection.getInputStream())));
                        StringBuffer sb = new StringBuffer();
                        String str;
                        while ((str = reader.readLine()) != null) {
                            str = str.replace("{", "");
                            str = str.replace("}", "");
                            str = str.replace("[", "");
                            str = str.replace("]", "");
                            str = str.replaceAll("\"", "");
                            System.out.println(str);
                            sb.append(str);
                        }
                     /*   getActivity().runOnUiThread(new Runnable(){

                            @Override
                            public void run() {
                                // hashedpsw=sb.toString();
                                //decrypt(hashedpsw);
                                if(sb.toString().contains("false")){
                                    invalidReg("Error","Invalid Credentials!");
                                }
                                else{
                                    showalert();
                                }
                            }
                        }); */
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // urlConnection.connect();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        Thread t = new Thread(r);
        t.start();
        UIUtil.hideKeyboard(this);
        comment.setText("");
        finish();
        startActivity(getIntent());

    }

    public void getComments(int ID) throws InterruptedException {
        Runnable r=new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("GET COMMENTS");
                    //URL url= new URL("https://students.btsi.lu/nodetheni/Attraction/"+attractionID);
                    URL url=new URL("https://192.168.131.234:10029/Comments"+"/"+ID);
                    HttpURLConnection urlConnection=(HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("GET");

                    /*OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
                    writer.flush();
                    writer.close();
                    out.close();  */
                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(urlConnection.getInputStream())));
                        StringBuffer sb = new StringBuffer();
                        String str;
                        while ((str = reader.readLine()) != null) {
                            System.out.println(str);
                            str=str.replace("{","");
                            str=str.replace("[","");
                            str=str.replace("]","");
                            str=str.replaceAll("\"","");
                            String[] values= str.split(",");
                            System.out.println(values[0]);
                            if(!values[0].equals("false")) {
                                for (int i = values.length-1; i >=0; i = i - 3) {
                                    String date = values[i];
                                    String day = date.substring(0, 10);
                                    String hour = date.substring(11, 19);
                                    System.out.println(day + " " + hour);
                                    //date=date.toString();
                                    //String[] dates=date.split("\\s+");
                                    String commentdate = day + " " + hour;
                                    Comment comment = new Comment(values[i-2], values[i - 1], commentdate);
                                    //System.out.println("Comment is "+comment.getDate());
                                    comments.add(comment);
                                }
                            }
                          else{
                                Comment comment=new Comment("","No comments here yet. Be the first one.","");
                                comments.add(comment);
                            }

                            // Picasso.get().load(values[2]).into(attractionimage);
                        }
                       /* runOnUiThread(new Runnable(){

                            @Override
                            public void run() {
                                // hashedpsw=sb.toString();
                                //decrypt(hashedpsw);
                                String imagepath=sb.toString();
                                System.out.println(imagepath);
                                Picasso.get().load(imagepath).into(attractionimage);
                            }
                        }); */
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // urlConnection.connect();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        Thread t=new Thread(r);
        t.start();
        t.join();
    }


    private static void disableSSLCertificateChecking() {
        TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {

                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {
                        // not implemented
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {
                        // not implemented
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                }
        };

        try {

            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {

                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }

            });
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}
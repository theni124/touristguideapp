package com.example.loginapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.input.InputManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.UiThread;
import androidx.fragment.app.Fragment;

import android.preference.PreferenceManager;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.sql.SQLOutput;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.security.cert.X509Certificate;

import at.favre.lib.crypto.bcrypt.BCrypt;
import at.favre.lib.crypto.bcrypt.BCryptFormatter;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginFragment extends Fragment {
    Button loginbutton;
    Button registerbutton;
    TextView text,forgotpassword;
    EditText editName,editPassword;
    private String username;
    private String password;
    private String plainpassword;
    private String hashedpsw;


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public LoginFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LoginFragment newInstance(String param1, String param2) {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // Defining the values for the input textfields and buttons
        View v= inflater.inflate(R.layout.fragment_login, container, false);
        editName  = (EditText) v.findViewById(R.id.usernameTextField);
        editPassword = (EditText) v.findViewById(R.id.passwordTextField);
        forgotpassword = (TextView) v.findViewById(R.id.forgotpasswordLabel);
        loginbutton= (Button) v.findViewById(R.id.loginbutton);
        text=(TextView) v.findViewById(R.id.textfield);
        registerbutton= (Button) v.findViewById(R.id.registerbutton);
        //Action event for the registration button
        registerbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoRegister();
            }
        });
        //Action event for the login button
        loginbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get text from EditText username Text Field
                username = editName.getText().toString();
                // get text from EditText password Text Field
                plainpassword = editPassword.getText().toString();
                //hide Keyboard
                UIUtil.hideKeyboard(getActivity());
                try {
                    postLogin(username,plainpassword);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
               // goToMain();
            }
        });
        //Action event for the forgot password textview
        forgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoForgotPsw();
            }
        });
        return v;
    }

    // opens the RegisterFragment
    public void gotoRegister(){
        RegisterFragment registerFragment=new RegisterFragment();
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.mainlayout,registerFragment).commit();
    }

    //opens the Forgot Password activity
    public void gotoForgotPsw()
    {
        Intent intent = new Intent (getActivity(), ForgotPasswordActivity.class);
        startActivity(intent);
    }

    //This method make a POST request to the Login method in the API with the username and password inside the body of the request
    public void postLogin(String username,String psw) throws JSONException {
        JSONObject json=new JSONObject();
        json.put("name",username);
        json.put("password",psw);
        String jsonString = json.toString();

        Runnable r=new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("Sending...");
                    URL url=new URL("https://192.168.131.234:10029/Login");

                    disableSSLCertificateChecking();

                    HttpsURLConnection urlConnection=(HttpsURLConnection) url.openConnection();
                    urlConnection.getSSLSocketFactory();
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setDoOutput(true);
                    urlConnection.addRequestProperty("Accept", "application/json");
                    urlConnection.addRequestProperty("Content-Type", "application/json");

                    OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
                    writer.write(jsonString);
                    writer.flush();
                    writer.close();
                    out.close();
                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(urlConnection.getInputStream())));
                        StringBuffer sb = new StringBuffer();
                        String str;
                        while ((str = reader.readLine()) != null) {
                            System.out.println(str);
                            str=str.replace("{","");
                            str=str.replace("}","");
                            str=str.replace("[","");
                            str=str.replace(":","");
                            str=str.replace("]","");
                            str=str.replaceAll("\"","");
                            str=str.replace("dtPassword","");
                            String[] values= str.split(",");
                            str=values[0];
                            System.out.println(str);
                            sb.append(str);
                        }
                        getActivity().runOnUiThread(new Runnable(){

                            @Override
                            public void run() {
                                if(sb.toString().contains("false")){
                                    text.setText("Invalid Login Credentials!");
                                }
                                else{
                                    goToMain();
                                    saveUsername(username);
                                }
                            }
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        Thread t=new Thread(r);
        t.start();
    }

        //opens the Main Activity
    public void goToMain(){
        Intent intent = new Intent (getActivity(), MainActivity.class);
        intent.putExtra("username",username);
        startActivity(intent);
    }

    //saves the username in the sharedPreferences
    public void saveUsername(String username){
        SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor myEditor = myPreferences.edit();
        myEditor.putString("Username", username);
        myEditor.commit();
    }

        //surpasses the SSL veryfication
    private static void disableSSLCertificateChecking() {
        TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {

                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {
                        // not implemented
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {
                        // not implemented
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                }
        };

        try {

            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {

                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }

            });
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}
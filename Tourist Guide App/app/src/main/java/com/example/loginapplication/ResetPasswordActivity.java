package com.example.loginapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class ResetPasswordActivity extends AppCompatActivity {
    EditText password1textfield,password2textfield;
    TextView text;
    Button continueButton;
    private String password1;
    private String password2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        //Define a value for the elements in the view
        text=(TextView) findViewById(R.id.textfield);
        password1textfield  = (EditText) findViewById(R.id.password1TextField);
        password2textfield = (EditText) findViewById(R.id.password2TextField);
        continueButton = (Button) findViewById(R.id.continuebutton);
        //Action event for the continue button
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                password1 = password1textfield.getText().toString();
                password2 = password2textfield.getText().toString();
                if(password1.equals("") || password2.equals("")){
                    text.setText("Fill out the fields");
                }
                else if(!password1.equals(password2)){
                    text.setText("Passwords do not match!");
                }
                else if(password1.length()<7 || password2.length()<7){
                    text.setText("Password must be at least 7 characters long");
                }
                else{
                    try {
                        resetPassword(password2);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    //This method make a POST request to the ResetPassword method in the API with the username and password inside the body of the request
        public void resetPassword(String password) throws JSONException {
            SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(ResetPasswordActivity.this);
            String name = myPreferences.getString("Username", "unknown");
            JSONObject json=new JSONObject();
            json.put("name",name);
            json.put("password",password);
            String jsonString = json.toString();

            Runnable r=new Runnable() {
                @Override
                public void run() {
                    try {
                        System.out.println("Sending...");
                        URL url=new URL("https://192.168.131.234:10029/ResetPassword");

                        disableSSLCertificateChecking();

                        HttpsURLConnection urlConnection=(HttpsURLConnection) url.openConnection();
                        urlConnection.getSSLSocketFactory();
                        urlConnection.setRequestMethod("POST");
                        urlConnection.setDoOutput(true);
                        urlConnection.addRequestProperty("Accept", "application/json");
                        urlConnection.addRequestProperty("Content-Type", "application/json");

                        OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
                        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
                        writer.write(jsonString);
                        writer.flush();
                        writer.close();
                        out.close();
                        try {
                            BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(urlConnection.getInputStream())));
                            StringBuffer sb = new StringBuffer();
                            String str;
                            while ((str = reader.readLine()) != null) {
                                System.out.println(str);
                                str=str.replace("{","");
                                str=str.replace("}","");
                                str=str.replace("[","");
                                str=str.replace(":","");
                                str=str.replace("]","");
                                str=str.replaceAll("\"","");
                                str=str.replace("dtPassword","");
                                String[] values= str.split(",");
                                str=values[0];
                                System.out.println(str);
                                sb.append(str);
                            }
                            runOnUiThread(new Runnable(){

                                @Override
                                public void run() {
                                    if(sb.toString().contains("false")){
                                        alert("Error","Something went wrong!");
                                    }
                                    else{
                                        alert("Success!","Your Password has been changed!");
                                    }
                                }
                            });
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            Thread t=new Thread(r);
            t.start();
        }

    public void goToLogin() {
        Intent intent = new Intent (this, LoginActivity.class);
        startActivity(intent);
    }


    //This method create an alert box with a custom title and message text
    public void alert(String title, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(msg);

        builder.setCancelable(false);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                goToLogin();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    private static void disableSSLCertificateChecking() {
        TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {

                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {
                        // not implemented
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {
                        // not implemented
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                }
        };

        try {

            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {

                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }

            });
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}
package com.example.loginapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentViewHolder> {
    private ArrayList<Comment> mcomments;
    public static class CommentViewHolder extends RecyclerView.ViewHolder{
        public TextView author;
        public TextView comment;
        public TextView date;

        public CommentViewHolder(@NonNull View itemView) {
            super(itemView);
            author= itemView.findViewById(R.id.authortext);
            comment= itemView.findViewById(R.id.commenttext);
            date = itemView.findViewById(R.id.date);
        }
    }

    public CommentAdapter(ArrayList<Comment> comments){
        mcomments=comments;
    }

    @NonNull
    @Override
    public CommentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v=LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_item,parent,false);
        CommentViewHolder cvh=new CommentViewHolder(v);
        return cvh;
    }

    @Override
    public void onBindViewHolder(@NonNull CommentViewHolder holder, int position) {
            Comment mycomment = mcomments.get(position);
            holder.author.setText(mycomment.getAuthor());
            holder.comment.setText(mycomment.getText());
            holder.date.setText(mycomment.getDate());
    }

    @Override
    public int getItemCount() {
        return mcomments.size();
    }
}

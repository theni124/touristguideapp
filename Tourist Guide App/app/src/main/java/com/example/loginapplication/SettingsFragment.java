package com.example.loginapplication;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.preference.PreferenceManager;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import at.favre.lib.crypto.bcrypt.BCrypt;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SettingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SettingsFragment extends Fragment {
    ListView listview;
    Button logout;
    private String newpassword;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public SettingsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SettingsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SettingsFragment newInstance(String param1, String param2) {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
      View v=inflater.inflate(R.layout.fragment_settings, container, false);
        listview=(ListView)v.findViewById(R.id.listview);
        logout= v.findViewById(R.id.logoutbutton);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmLogout();
            }
        });

        ArrayList<String> settings=new ArrayList<>();
        settings.add("Change Username");
        settings.add("Change Password");
        settings.add("Change E-Mail");

        ArrayAdapter arrayadapter=new ArrayAdapter(getActivity(),android.R.layout.simple_list_item_1,settings);
        listview.setAdapter(arrayadapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                //System.out.println("YE");
                System.out.print("Position is" +position);
                if(position==0){
                    showAlert(settings.get(position).toString(),"Enter new Username:");
                }
                else if(position==1){
                    showAlert(settings.get(position).toString(),"Enter new Passsword:");
                }
                else if(position==2){
                    showAlert(settings.get(position).toString(),"Enter new E-Mail:");
                }
                //Toast.makeText(getActivity(),"clicked item:"+position+" "+settings.get(position).toString(),Toast.LENGTH_SHORT).show();
            }
        });
      return v;
    }

    public void showAlert(String title, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
        builder.setTitle(title);
        builder.setMessage(msg);

        builder.setCancelable(true);

        final EditText input = new EditText(getActivity());
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);


        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                    if(title.equals("Change Username")){
                        try {
                            editUsername(input.getText().toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    else if(title.equals("Change Password")){
                        if(checkPassword(input.getText().toString())) {
                            try {
                                editPassword(input.getText().toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else{
                            showMessage("Error","Password must be at least 7 characters long!");
                        }
                    }
                    else if(title.equals("Change E-Mail")){
                        if(checkMail(input.getText().toString())) {
                            try {
                                editMail(input.getText().toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else{
                            showMessage("Error","Invalid E-Mail format!");
                        }
                    }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void confirmLogout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
        builder.setTitle("Logout");
        builder.setMessage("Do you really want to Logout?");
        builder.setCancelable(true);

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //goToLogin();
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void editUsername(String newusername) throws JSONException {
        SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        String oldusername = myPreferences.getString("Username", "unknown");
        JSONObject json=new JSONObject();
       // String userpsw=encrypt2(psw);
        json.put("oldusername",oldusername);
        json.put("newusername",newusername);
        String jsonString = json.toString();

        Runnable r=new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("Edit User...");
                    //URL url= new URL("https://students.btsi.lu/nodetheni/Login");
                    URL url=new URL("https://192.168.131.234:10029/EditUser");

                    disableSSLCertificateChecking();

                    HttpsURLConnection urlConnection=(HttpsURLConnection) url.openConnection();
                    urlConnection.getSSLSocketFactory();
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setDoOutput(true);
                    urlConnection.addRequestProperty("Accept", "application/json");
                    urlConnection.addRequestProperty("Content-Type", "application/json");

                    OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
                    writer.write(jsonString);
                    writer.flush();
                    writer.close();
                    out.close();
                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(urlConnection.getInputStream())));
                        StringBuffer sb = new StringBuffer();
                        String str;
                        while ((str = reader.readLine()) != null) {
                            str=str.replace("{","");
                            str=str.replace("}","");
                            str=str.replace("[","");
                            str=str.replace(":","");
                            str=str.replace("]","");
                            str=str.replaceAll("\"","");
                            str=str.replace("dtPassword","");
                            String[] values= str.split(",");
                            str=values[0];
                            System.out.println(str);
                            sb.append(str);
                        }
                        getActivity().runOnUiThread(new Runnable(){

                            @Override
                            public void run() {
                                // hashedpsw=sb.toString();
                                //decrypt(hashedpsw);
                                if(sb.toString().equals("name_changedname_changed")){
                                    //save Username
                                    SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                                    SharedPreferences.Editor myEditor = myPreferences.edit();
                                    myEditor.putString("Username", newusername);
                                    myEditor.commit();

                                   showMessage("Success!","Your username has been changed!");
                                }
                                else{
                                    showMessage("Error!","Something went wrong...");
                                }
                            }
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // urlConnection.connect();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        Thread t=new Thread(r);
        t.start();
    }

    public void editPassword(String newpassword) throws JSONException {
        SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        String oldusername = myPreferences.getString("Username", "unknown");
        JSONObject json=new JSONObject();
        // String userpsw=encrypt2(psw);
        json.put("oldusername",oldusername);
        json.put("newpassword",newpassword);
        String jsonString = json.toString();

        Runnable r=new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("Edit User...");
                    //URL url= new URL("https://students.btsi.lu/nodetheni/Login");
                    URL url=new URL("https://192.168.131.234:10029/EditUser");

                    disableSSLCertificateChecking();

                    HttpsURLConnection urlConnection=(HttpsURLConnection) url.openConnection();
                    urlConnection.getSSLSocketFactory();
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setDoOutput(true);
                    urlConnection.addRequestProperty("Accept", "application/json");
                    urlConnection.addRequestProperty("Content-Type", "application/json");

                    OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
                    writer.write(jsonString);
                    writer.flush();
                    writer.close();
                    out.close();
                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(urlConnection.getInputStream())));
                        StringBuffer sb = new StringBuffer();
                        String str;
                        while ((str = reader.readLine()) != null) {
                            str=str.replace("{","");
                            str=str.replace("}","");
                            str=str.replace("[","");
                            str=str.replace(":","");
                            str=str.replace("]","");
                            str=str.replaceAll("\"","");
                            str=str.replace("dtPassword","");
                            String[] values= str.split(",");
                            str=values[0];
                            System.out.println(str);
                            sb.append(str);
                        }
                        getActivity().runOnUiThread(new Runnable(){

                            @Override
                            public void run() {
                                // hashedpsw=sb.toString();
                                //decrypt(hashedpsw);
                                if(sb.toString().equals("password_changedpassword_changed")){
                                    //save Username
                                   /* SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                                    SharedPreferences.Editor myEditor = myPreferences.edit();
                                    myEditor.putString("Username", newusername);
                                    myEditor.commit(); */

                                    showMessage("Success!","Your Password has been changed!");
                                }
                                else{
                                    showMessage("Error!","Something went wrong...");
                                }
                            }
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // urlConnection.connect();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        Thread t=new Thread(r);
        t.start();
    }

    public void editMail(String newmail) throws JSONException {
        SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        String oldusername = myPreferences.getString("Username", "unknown");
        JSONObject json=new JSONObject();
        json.put("newmail",newmail);
        // String userpsw=encrypt2(psw);
        json.put("oldusername",oldusername);
        String jsonString = json.toString();

        Runnable r=new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("Edit User...");
                    //URL url= new URL("https://students.btsi.lu/nodetheni/Login");
                    URL url=new URL("https://192.168.131.234:10029/EditUser");

                    disableSSLCertificateChecking();

                    HttpsURLConnection urlConnection=(HttpsURLConnection) url.openConnection();
                    urlConnection.getSSLSocketFactory();
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setDoOutput(true);
                    urlConnection.addRequestProperty("Accept", "application/json");
                    urlConnection.addRequestProperty("Content-Type", "application/json");

                    OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
                    writer.write(jsonString);
                    writer.flush();
                    writer.close();
                    out.close();
                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(urlConnection.getInputStream())));
                        StringBuffer sb = new StringBuffer();
                        String str;
                        while ((str = reader.readLine()) != null) {
                            str=str.replace("{","");
                            str=str.replace("}","");
                            str=str.replace("[","");
                            str=str.replace(":","");
                            str=str.replace("]","");
                            str=str.replaceAll("\"","");
                            str=str.replace("dtPassword","");
                            String[] values= str.split(",");
                            str=values[0];
                            System.out.println(str);
                            sb.append(str);
                        }
                        getActivity().runOnUiThread(new Runnable(){

                            @Override
                            public void run() {
                                // hashedpsw=sb.toString();
                                //decrypt(hashedpsw);
                                if(sb.toString().equals("mail_changedmail_changed")){
                                    //save Username
                                   /* SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                                    SharedPreferences.Editor myEditor = myPreferences.edit();
                                    myEditor.putString("Username", newusername);
                                    myEditor.commit(); */

                                    showMessage("Success!","Your E-Mail has been changed!");
                                }
                                else{
                                    showMessage("Error!","Something went wrong...");
                                }
                            }
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // urlConnection.connect();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        Thread t=new Thread(r);
        t.start();
    }

    public boolean checkPassword(String psw){
        boolean isValid;
        if(psw.length()<7){
            isValid=false;
        }else {
            isValid=true;
        }
        return isValid;
    }

    public boolean checkMail(String mail){
        boolean isvalid;
        Pattern pattern = Pattern.compile("^[a-zA-Z0-9][a-zA-Z0-9._-]*@[a-zA-Z0-9][a-zA-Z0-9._-]*\\.[a-zA-Z]{2,4}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(mail);
        boolean matchFound = matcher.find();
        if(matchFound){
            isvalid=true;
        }
        else{
            isvalid=false;
        }
        return isvalid;
    }

    private static void disableSSLCertificateChecking() {
        TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {

                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {
                        // not implemented
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {
                        // not implemented
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                }
        };

        try {

            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {

                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }

            });
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public void showMessage(String title, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
        builder.setTitle(title);
        builder.setMessage(msg);

        builder.setCancelable(false);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //goToLogin();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }
}
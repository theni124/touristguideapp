package com.example.loginapplication;

public class Comment {
    private String author;
    private String text;
    private String date;

    public Comment(String pAuthor,String pText,String pDate){
        author=pAuthor;
        text=pText;
        date=pDate;
    }

    public String getAuthor(){return author;}
    public String getText(){return text;}
    public String getDate(){return date;}
}

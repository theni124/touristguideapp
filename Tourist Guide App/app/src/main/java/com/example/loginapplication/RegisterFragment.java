package com.example.loginapplication;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import at.favre.lib.crypto.bcrypt.BCrypt;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RegisterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RegisterFragment extends Fragment {

    Button newUserButton;
    Button cancelButton;
    EditText newusernameTextField, newpasswordTextField, newemailTextField;

    private String newusername;
    private String newpassword;
    private String newemail;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public RegisterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RegisterFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RegisterFragment newInstance(String param1, String param2) {
        RegisterFragment fragment = new RegisterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_register, container, false);
        //DEfine the elements on the view
        newusernameTextField = (EditText) v.findViewById(R.id.newUsernameTextField);
        newpasswordTextField = (EditText) v.findViewById(R.id.newPasswordTextField);
        newemailTextField = (EditText) v.findViewById(R.id.newEmailTextField);
        newUserButton = (Button) v.findViewById(R.id.newUserButton);
        cancelButton = (Button) v.findViewById(R.id.cancelButton);
        //Action event for the new user button
        newUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newusername = newusernameTextField.getText().toString();
                newpassword = newpasswordTextField.getText().toString();
                newemail = newemailTextField.getText().toString();
                UIUtil.hideKeyboard(getActivity());
                if (newusername.isEmpty() || newpassword.isEmpty() || newemail.isEmpty()) {
                    invalidReg("Error", "Please fill out all the fields!");
                }
                else if(newpassword.length()<7){
                    invalidReg("Error", "Password must be at least 7 characters long!");
                } else {
                    if(checkMail(newemail)) {
                        try {
                            createnewuser(newusername, newpassword, newemail);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    else{
                        invalidReg("Error", "E-Mail is not valid!");
                    }
                }
            }
        });
        //Action Event for ht cancel button
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToLogin();
            }
        });
        return v;
    }

    public void goToLogin() {
        LoginFragment loginFragment = new LoginFragment();
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.mainlayout, loginFragment).commit();
    }

    //This method make a POST request to the Register method in the API with the username,password and email inside the body of the request
    public void createnewuser(String username,String psw,String email) throws JSONException {
        JSONObject json=new JSONObject();
        json.put("username",username);
        json.put("password",psw);
        json.put("email",email);
        String jsonString = json.toString();

        Runnable r=new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("Sending...");
                    URL url=new URL("https://192.168.131.234:10029/NewUser");
                    disableSSLCertificateChecking();
                    HttpsURLConnection urlConnection=(HttpsURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setDoOutput(true);
                    urlConnection.addRequestProperty("Accept", "application/json");
                    urlConnection.addRequestProperty("Content-Type", "application/json");

                    OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
                    writer.write(jsonString);
                    writer.flush();
                    writer.close();
                    out.close();
                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(urlConnection.getInputStream())));
                        StringBuffer sb = new StringBuffer();
                        String str;
                        while ((str = reader.readLine()) != null) {
                            str=str.replace("{","");
                            str=str.replace("}","");
                            str=str.replace("[","");
                            str=str.replace("]","");
                            str=str.replaceAll("\"","");
                            System.out.println(str);
                            sb.append(str);
                        }
                        getActivity().runOnUiThread(new Runnable(){

                            @Override
                            public void run() {
                                if(sb.toString().contains("false")){
                                    invalidReg("Error","Invalid Credentials!");
                                }
                                else{
                                    showalert();
                                }
                            }
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // urlConnection.connect();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        Thread t=new Thread(r);
        t.start();
    }

    //this methodd creates an alert box when the user has susccessfully created a new user account
    public void showalert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
        builder.setMessage("You can now login.");

        builder.setTitle("New User created!");

        builder.setCancelable(false);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                goToLogin();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    //this method creates an alert box with custom title and messsage text
    public void invalidReg(String title, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
        builder.setTitle(title);
        builder.setMessage(msg);

        builder.setCancelable(false);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //goToLogin();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    //This method checks if the entered email has a correct email format
    public boolean checkMail(String mail){
        boolean isvalid;
        Pattern pattern = Pattern.compile("^[a-zA-Z0-9][a-zA-Z0-9._-]*@[a-zA-Z0-9][a-zA-Z0-9._-]*\\.[a-zA-Z]{2,4}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(mail);
        boolean matchFound = matcher.find();
        if(matchFound){
            isvalid=true;
        }
        else{
            isvalid=false;
        }
        return isvalid;
    }

    private static void disableSSLCertificateChecking() {
        TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {

                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {
                        // not implemented
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {
                        // not implemented
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                }
        };

        try {

            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {

                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }

            });
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}